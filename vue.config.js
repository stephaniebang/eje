const path = require('path')
const alias = location => path.resolve(__dirname, location)

module.exports = {
  chainWebpack: (config) => {
    // path aliases
    config.resolve.alias
      .set('@components', alias('src/components'))
      .set('@containers', alias('src/containers'))
      .set('@content', alias('src/content'))
      .set('@assets', alias('src/assets'))
      .set('@helpers', alias('src/helpers.js'))

    // svg loader
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .use('vue-svg-loader')
      .loader('vue-svg-loader')
  },

  // style
  css: {
    loaderOptions: {
      sass: { data: '@import "src/style.scss";' }
    }
  }
}
