export default {
  description: "eJê is a tool that offers custom resources for the creation of new works of art that have the artist Jê Américo's aesthetic identity.",
  authors: ["André Luiz Akabane Solak", "Stephanie Eun Ji Bang"],
  url: "https://linux.ime.usp.br/~stephanie/mac0499/"
}