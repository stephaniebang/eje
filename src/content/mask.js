export default {
  type: {
    label: 'Shape',
    type: 'select',
    options: ['rectangle']
  },

  rectangle: {
    position: {
      title: 'Position',
      fields: {
        x: {
          label: 'x',
          type: 'number'
        },
        y: {
          label: 'y',
          type: 'number'
        }
      },
      row: true
    },

    size: {
      title: 'Size',
      fields: {
        width: {
          label: 'w',
          type: 'number'
        },
        height: {
          label: 'h',
          type: 'number'
        }
      },
      row: true
    }
  },

  // these circle settings are for tests purposes for now
  circle: {
    position: {
      title: 'Center position',
      fields: {
        x: {
          label: 'x',
          type: 'number'
        },
        y: {
          label: 'y',
          type: 'number'
        }
      },
      row: true
    },

    size: {
      title: 'Size',
      fields: {
        diameter: {
          label: 'diameter',
          type: 'Number'
        }
      },
      row: true
    }
  },

  blockedText: 'Masks are unavailable for the base layer. Please, switch to a higher layer or add a new one to apply masks.'
}
