export default {
  resolution: {
    parameter: {
      title: 'Resolution',
      fields: {
        width: {
          label: 'w',
          type: 'number'
        },
        height: {
          label: 'h',
          type: 'number'
        }
      },
      row: true
    },
    options: {
      screen: {
        title: 'Screen',
        values: {
          hd: {
            name: 'HD',
            width: 1280,
            height: 720
          },
          fhd: {
            name: 'Full HD',
            width: 1920,
            height: 1080
          },
          qhd: {
            name: 'Quad HD',
            width: 2560,
            height: 1440
          },
          k2: {
            name: '2K',
            width: 2048,
            height: 1080
          },
          k4: {
            name: '4K',
            width: 3840,
            height: 2169
          }
        }
      },
      paperPortrait: {
        title: 'Paper Portrait',
        values: {
          a0: {
            name: 'A0',
            width: 841,
            height: 1189
          },
          a1: {
            name: 'A1',
            width: 594,
            height: 841
          },
          a2: {
            name: 'A2',
            width: 420,
            height: 594
          },
          a3: {
            name: 'A3',
            width: 297,
            height: 420
          },
          a4: {
            name: 'A4',
            width: 210,
            height: 297
          }
        }
      },
      paperLandscape: {
        title: 'Paper Landscape',
        values: {
          a0: {
            name: 'A0',
            height: 841,
            width: 1189
          },
          a1: {
            name: 'A1',
            height: 594,
            width: 841
          },
          a2: {
            name: 'A2',
            height: 420,
            width: 594
          },
          a3: {
            name: 'A3',
            height: 297,
            width: 420
          },
          a4: {
            name: 'A4',
            height: 210,
            width: 297
          }
        }
      }
    }
  }
}
