export default {
  thickness: {
    title: 'Thickness',
    fields: {
      thickness: {
        label: 'Mean',
        type: 'number'
      },
      probThickName: {
        label: 'Distribution',
        type: 'select',
        options: ['Uniforme', 'Homogêneo', 'Normal', 'Exponencial', 'Poisson']
      }
    }
  },

  distance: {
    title: 'Distance',
    fields: {
      distance: {
        label: 'Mean',
        type: 'number'
      },
      probDistName: {
        label: 'Distribution',
        type: 'select',
        options: ['Uniforme', 'Homogêneo', 'Normal', 'Exponencial', 'Poisson'],
        action: 'refresh'
      }
    }
  },

  color: {
    title: 'Color',
    fields: {
      colorPicker: {
        type: 'color'
      }
    }
  }
}