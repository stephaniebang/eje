import Vue from 'vue'
import Vuex from 'vuex'

import display from './modules/display'
import layers from './modules/layers'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    display,
    layers
  },

  getters: {
    lines: state => {
      return state.layers.list[state.layers.index][state.layers.linesType].list
    },
    masks: state => {
      return state.layers.list[state.layers.index].masks.list
    },
    line: state => {
      const lines = state.layers.list[state.layers.index][state.layers.linesType]

      return lines.list[lines.index]
    },
    mask: state => {
      const masks = state.layers.list[state.layers.index].masks

      return masks.list[masks.index]
    }
  }
})
