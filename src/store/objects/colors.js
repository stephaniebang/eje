
function Colors (params) {
  this.list = params ? params.list : []

  this.addColor = (color) => {
    this.list.push(color)
  }

  this.updateColor = (color, index) => {
    this.list.splice(index, 1, color)
  }
}

export default Colors
