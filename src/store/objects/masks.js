import { treatedValue } from '../helpers'

const rectangle = () => ({
  position: { x: 0, y: 0 },
  size: { width: 0, height: 0 }
})

const circle = () => ({
  position: { x: 0, y: 0 },
  size: { diameter: 0 }
})


function Masks (params) {
  this.list = params ? params.list : []
  // for now, there's only the rectangle shape
  // this.shape = params ? params.shape : ''
  this.shape = 'rectangle'
  this.counter = params ? params.counter : 1

  this.addMask = () => {
    const shape = this.shape === 'rectangle'
      ? rectangle()
      : circle()
    /* ------------------------
    INSERE ORDENADAMENTE AQUI
    -------------------------- */
    this.list.push({
      ...shape,
      id: this.counter++
    })
  },

  this.removeMask = id => {
    const index = this.list.findIndex(mask => mask.id === id)

    this.list.splice(index, 1)
  }

  this.updateMask = ({ id, name, param, value }) => {
    const val = treatedValue(name, value)
    const index = this.list.findIndex(mask => mask.id === id)

    this.list[index][param][name] = val
  }

  this.updateShape = shape => {
    this.shape = shape
    this.list = []
    this.counter = 1
  }

  this.fixSize = id => {
    const index = this.list.findIndex(mask => mask.id === id)
    const size = this.list[index].size

    if (size.width < 0) {
      this.list[index].position.x += size.width
      size.width = -size.width
    }

    if (size.height < 0) {
      this.list[index].position.y += size.height
      size.height = -size.height
    }
  }
}

export default Masks
