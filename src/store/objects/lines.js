import { getSample, treatedValue } from '../helpers'

const lineEnd = line => {
  if (line.lineSet.length) return line.lineSet[0].y + line.lineSet[0].thickness
  if (line.thickness < line.distance) return line.y + line.distance
  return line.y + line.thickness
}

const isLine = (y, lines, index) => {
  let found = false

  lines.some((line, ind) => {
    if (ind === index) return true

    if (line.lineSet.length) {
      return line.lineSet.some(randomLine => {
        if (randomLine.y <= y && randomLine.y + randomLine.thickness >= y) {
          found = true
          return true
        }

        return false
      })
    }

    return false
  })

  return found
}

const newY = (y, lines, index, max) => {
  let newY = max

  lines.some((line, ind) => {
    if (ind === index) return true

    if (line.lineSet.length) line.lineSet.some(randomLine => {
      const lineEnd = randomLine.y + randomLine.thickness

      if (lineEnd <= y || isLine(lineEnd + 1, lines, index)) return false

      if (lineEnd < newY) newY = lineEnd

      return true
    })

    return false
  })

  return newY
}


function Lines (params) {
  this.list = params ? params.list : [{
    thickness: 0,
    distance: 1,
    windup: 0,
    color: '#FF0000FF',
    probThickName: '',
    probDistName: '',
    lineSet: [],
    y: 0
  }]
  this.index = params ? params.index : 0

  this.updateLine = (name, value, height) => {
    const currentLine = this.list[this.index]
    const val = treatedValue(name, value)

    currentLine[name] = val

    if (name === 'thickness' && currentLine.thickness > currentLine.distance)
      currentLine.distance = currentLine.thickness

    if (name === 'probDistName' || name === 'probThickName')
      this.updateLineSet(name, height)
    else if (name === 'distance' && currentLine.lineSet.length)
      this.updateLineSet('probDistName', height)
    else if (name === 'thickness' && currentLine.lineSet.length)
      this.updateLineSet('probThickName', height)
  }

  this.updateIndex = (index) => {
    this.index = index
  }

  this.addLine = (height) => {
    const line = this.list[this.index]
    const y = lineEnd(line)
    const newLine = {
      thickness: 0,
      distance: 1,
      windup: 0,
      color: '#FF0000FF',
      probThickName: '',
      probDistName: '',
      lineSet: [],
      y: isLine(y, this.list, this.index)
        ? newY(y, this.list, this.index, height)
        : y
    }

    // if we're adding at the top of params
    if (this.index+1 === this.list.length) {
      this.list.push(newLine)
    }
    else {
      this.list = this.list.slice(0, this.index+1)
      this.list.push(newLine)
    }
  }

  this.swapLines = (operation, height) => {
    const line = this.list[this.index]
    const y = operation === -1 ? lineEnd(line) : line.y
    // new element must be updated in every undo due to parameter y
    const newLine = {
      thickness: 0,
      distance: 1,
      windup: 0,
      color: '#FF0000FF',
      probThickName: '',
      probDistName: '',
      lineSet: [],
      // Precisa arrumar o cálculo da próxima linha (Trello)
      // y: isLine(y, this.list, this.index)
      //   ? newY(y, this.list, this.index, height)
      //   : y
      y
    }

    // if we're handling the first undo, we must create a new params element
    if (this.index+2 === this.list.length && operation > -1) {
      this.list.splice(this.index, 0, newLine)
    }
    // just swap elements
    else {
      this.list.splice(this.index + operation, 1, line)
      this.list.splice(this.index, 1, newLine)
    }
  }

  this.removeOldLines = () => {
    this.list = this.list.slice(0, this.index + 1)
  }

  this.updateLineSet = (name, height, newDistribution, lineIndex) => {
    const line = this.list[lineIndex || this.index]
    let y = line.y
    let ind = 0

    if (!line.probDistName) return line.lineSet = []

    // if there is already a distribution set for the other param, update it
    line.lineSet = line.lineSet.length && !newDistribution
      ? line.lineSet.map((el, index) => {
          if (y < height) {
            ind++
            if (!y) {
              y++
              return {
                y: 0,
                thickness: name === 'probDistName'
                  ? el.thickness
                  : getSample(line.probThickName, line.thickness)
              }
            }
            y = name === 'probDistName'
              ? y + (index ? getSample(line.probDistName, line.distance) : 0)
              : el.y

            return {
              y,
              thickness: name === 'probDistName'
                ? el.thickness
                : getSample(line.probThickName, line.thickness)
            }
          }

          return el
        })
      : [{ y, thickness: getSample(line.probThickName, line.thickness) }]

    // remove unnecessary elements in lineSet
    if (y >= height) {
      line.lineSet = line.lineSet.slice(0, ind)
    }

    // add new elements to lineSet, if needed
    while (y < height) {
      y += getSample(line.probDistName, line.distance)
      line.lineSet.push({
        y,
        thickness: getSample(line.probThickName, line.thickness)
      })
    }
  }

  this.updateList = (list) => {
    this.list = list
  }
}

export default Lines
