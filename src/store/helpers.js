import Prob from 'prob.js'

export const getSample = (name, distance) => {
  if (name === 'Normal') {
    return ~~(Prob.normal(distance, distance*.25)())
  }
  if (name === 'Homogêneo') {
    return ~~(Prob.uniform(distance - 5, distance + 5)())
  }
  if (name === 'Exponencial') {
    return ~~(Prob.exponential(2.5/distance)()) + 1
  }
  if (name === 'Poisson') {
    return ~~(Prob.poisson(distance*0.5)())
  }
  return distance
}

export const treatedValue = (name, value) => {
  if (!value || isNaN(value)) return value

  const val = +value

  if (name === 'distance') return val < 1 ? 1 : val
  if (name === 'thickness') return val < 0 ? 0 : val
  return val
}
