import { treatedValue } from '../helpers'

const state = {
  // canvas resolution
  width: 1500,
  height: 1500,
  // current menu section
  section: 'settings'
}

const mutations = {
  updateSection: (state, section) => {
    state.section = section
  },

  updateWidth: (state, width) => {
    state.width = width
  },

  updateHeight: (state, height) => {
    state.height = height
  }
}

const actions = {
  updateSection: ({ commit }, section) => {
    commit('updateSection', section)
    commit('updateLinesType', section === 'layers' ? 'background' : 'foreground', { root: true })
  },

  updateResolution: ({ commit }, param) => {
    const value = treatedValue(param.name, param.value)

    if (param.name === 'width') commit('updateWidth', value)
    else commit('updateHeight', value)
  },

  replaceResolution: ({ commit }, resolution) => {
    commit('updateWidth', resolution.width)
    commit('updateHeight', resolution.height)
  }
}

export default { state, mutations, actions }
