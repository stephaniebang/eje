import Lines from '../objects/lines'
import Masks from '../objects/masks'
import Colors from '../objects/colors'

// state holds the list of elements (lines and masks) of the Canva
const state = {
  list: [{
    name: 'Layer-1',
    masks: new Masks(),
    background: new Lines(),
    foreground: new Lines(),
    hidden: false
  }],
  colors: new Colors(),
  backgroundColor: '',
  index: 0,
  counter: 1,
  linesType: 'background'
}

const mutations = {
  /* Layers */
  addLayer: (state) => state.list.push({
    name: `Layer-${++state.counter}`,
    masks: new Masks(),
    background: new Lines(),
    foreground: new Lines(),
    hidden: false
  }),

  removeLayer: (state, index) => {
    state.list.splice(index, 1)
  },

  replaceLayer: (state, { layer, index }) => {
    state.list.splice(index, 1, {
      name: layer.name,
      masks: new Masks({
        list: layer.masks.list,
        shape: layer.masks.shape,
        counter: layer.masks.counter
      }),
      background: new Lines({
        list: layer.background.list,
        index: layer.background.index
      }),
      foreground: new Lines({
        list: layer.foreground.list,
        index: layer.foreground.index
      }),
      hidden: false
    })
  },

  replaceState: (state, layers) => {
    state.list = []

    layers.list.forEach(layer => state.list.push({
      name: layer.name,
      masks: new Masks({
        list: layer.masks.list,
        shape: layer.masks.shape,
        counter: layer.masks.counter
      }),
      background: new Lines({
        list: layer.background.list,
        index: layer.background.index
      }),
      foreground: new Lines({
        list: layer.foreground.list,
        index: layer.foreground.index
      }),
      hidden: false
    }))

    state.colors = new Colors({
      list: layers.colors.list,
      index: layers.colors.index
    })
    state.backgroundColor = layers.backgroundColor
    state.index = layers.list.length ? layers.list.length - 1 : 0
    state.counter = layers.counter
    state.linesType = layers.linesType
  },

  updateIndex: (state, index) => {
    state.index = index
  },

  updateLinesType: (state, type) => {
    state.linesType = type
  },

  updateVisibility: (state, index) => {
    state.list[index].hidden = !state.list[index].hidden
  },

  /* Masks */
  updateMaskShape: (state, shape) => {
    const currentMasks = state.list[state.index].masks
    
    currentMasks.updateShape(shape)
  },

  addMask: state => {
    const currentMasks = state.list[state.index].masks
    
    currentMasks.addMask()
  },

  updateMask: (state, value) => {
    const currentMasks = state.list[state.index].masks

    currentMasks.updateMask(value)
  },

  deleteMask: (state, id) => {
    const currentMasks = state.list[state.index].masks

    currentMasks.removeMask(id)
  },

  fixSize: (state, id) => {
    const currentMasks = state.list[state.index].masks

    currentMasks.fixSize(id)
  },
  
  /* Lines */
  updateLine: (state, { name, value, height }) => {
    const lines = state.list[state.index][state.linesType]

    lines.updateLine(name, value, height)
  },

  updateLineIndex: (state, index) => {
    const lines = state.list[state.index][state.linesType]

    lines.updateIndex(index)
  },

  addLine: (state, height) => {
    const lines = state.list[state.index][state.linesType]

    lines.addLine(height)
  },

  swapLines: (state, { operation, height }) =>  {
    const lines = state.list[state.index][state.linesType]

    lines.swapLines(operation, height)
  },

  removeOldLines: (state) => {
    const lines = state.list[state.index][state.linesType]

    lines.removeOldLines()
  },

  updateLineSet: (state, { newDistribution, height, specificLine }) => {
    const lines = specificLine
      ? state.list[specificLine.layer][specificLine.type]
      : state.list[state.index][state.linesType]
    const index = specificLine && specificLine.line

    lines.updateLineSet(null, height, newDistribution, index)
  },

  updateLines: (state, lines) => {
    const oldLines = state.list[state.index][state.linesType]

    oldLines.updateList(lines)
  },

  updateBackground: (state, color) => {
    state.backgroundColor = color
  },

  /* Colors */
  addColor: (state, color) => {
    state.colors.addColor(color)
  },

  updateColor: (state, { index, color }) => {
    state.colors.updateColor(color, index)
  }
}

const actions = {
  /* Layers */
  addLayer: ({ commit }) => {
    commit('addLayer')
  },

  removeLayer: ({ commit, state }, level) => {
    if (state.index && state.index >= state.list.length - 1) {
      commit('updateIndex', state.list.length - 2)
    }

    commit('removeLayer', level)
  },

  updateLayerInd: ({ commit }, index) => {
    commit('updateIndex', index)
  },

  replaceLayer: ({ commit }, value) => {
    commit('replaceLayer', value)
  },

  replaceState: ({ commit }, layers) => {
    commit('replaceState', layers)
  },

  updateVisibility: ({ commit }, index) => {
    commit('updateVisibility', index)
  },

  /* Masks */
  updateMaskShape: ({ commit }, shape) => {
    commit('updateMaskShape', shape)
  },

  addMask: ({ commit }) => {
    commit('addMask')
  },

  updateMask: ({ commit }, value) => {
    commit('updateMask', value)
  },

  deleteMask: ({ commit }, id) => {
    commit('deleteMask', id)
  },

  fixSize: ({ commit }, id) => {
    commit('fixSize', id)
  },

  /* Lines */
  updateLine: ({ commit, rootState }, { name, value }) => {
    commit('updateLine', { name, value, height: rootState.display.height })
  },

  newDistribution: async ({ commit, rootState }, specificLine) => {
    commit('updateLineSet', { newDistribution: true, height: rootState.display.height, specificLine })
  },

  addLine: ({ commit, state, rootState }) => {
    const currentLines = state.list[state.index][state.linesType]

    if (currentLines.list.length > currentLines.index + 1) commit('removeOldLines')
    commit('addLine', rootState.display.height)
    commit('updateLineIndex', currentLines.index + 1)
  },

  undo: ({ commit, state, rootState }) => {
    const currentLines = state.list[state.index][state.linesType]

    if (currentLines.index > 0) {
      commit('updateLineIndex', currentLines.index - 1)
      commit('swapLines', { operation: 1, height: rootState.display.height })
    }
  },

  redo: ({ commit, state, rootState }) => {
    const currentLines = state.list[state.index][state.linesType]

    if (currentLines.list.length > currentLines.index + 2) {
      commit('updateLineIndex', currentLines.index + 1)
      commit('swapLines', { operation: -1, height: rootState.display.height })
    }
  },

  replaceLines: ({ commit, dispatch }, newLines) => {
    commit('updateLines', newLines)
    commit('updateLineIndex', newLines.length - 1)
    dispatch('addLine')
  },

  updateBackground: ({ commit }, color) => {
    commit('updateBackground', color)
  },

  /* Colors */
  addColor: ({ commit }, color) => {
    commit('addColor', color)
  },

  updateColor: ({ commit }, { index, color }) => {
    commit('updateColor', { index, color })
  }
}

export default { state, mutations, actions }
